﻿using BlogApiDotNet.Data;
using BlogApiDotNet.DTO;
using BlogApiDotNet.Models;
using Microsoft.CodeAnalysis.Text;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BlogApiDotNet.Repositories
{
    public class BlogRepository : IBlogRepository, IDisposable
    {
        private readonly EntityContext _context;
        private static readonly List<string> LISTPOS = new List<string>(new[] { "Viet Nam", "Chau A", "Chau Au", "Chau My" });
        public BlogRepository(EntityContext context)
        {
            _context = context;
        }

        public object CreateBlog(BlogDTO blogDTO)
        {
            List<int> listPosition = blogDTO.listposition.Split(',').Select(Int32.Parse).ToList();
            Blog blog = new Blog()
            {
                Id = 0,
                title = blogDTO.title,
                description = blogDTO.description,
                detail = blogDTO.detail,
                thumb = blogDTO.thumb,
                datePublic = blogDTO.datePublic,
                status = blogDTO.status,
                CategoryId = blogDTO.CategoryId,
                category = _context.Categories.Find(blogDTO.CategoryId),
                Positions = null,
            };
            _context.Blogs.Add(blog);
            _context.SaveChanges();

            AddPosition(listPosition, blog);
            _context.SaveChanges();

            return blog;
        }

        public object DeleteBlogById(int id)
        {
            Blog blog = _context.Blogs.Find(id);
            if (blog == null)
            {
                return null;
            }
            _context.Positions.RemoveRange(_context.Positions.Where(x => x.BlogId == id).ToList());
            _context.Blogs.Remove(blog);
            _context.SaveChanges();
            return blog;
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public object EditBlogById(int id, BlogDTO blogDTO)
        {
            List<int> listPosition = blogDTO.listposition.Split(',').Select(Int32.Parse).ToList();
            if (id != blogDTO.Id)
            {
                return null;
            }
            Blog blog = new Blog()
            {
                Id = blogDTO.Id,
                title = blogDTO.title,
                description = blogDTO.description,
                detail = blogDTO.detail,
                thumb = blogDTO.thumb,
                datePublic = blogDTO.datePublic,
                status = blogDTO.status,
                CategoryId = blogDTO.CategoryId,
                category = _context.Categories.Find(blogDTO.CategoryId),
                Positions = null,
            };
            _context.Entry(blog).State = EntityState.Modified;
            _context.Positions.RemoveRange(_context.Positions.Where(x => x.BlogId == id).ToList());

            AddPosition(listPosition, blog);
            try
            {
                _context.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (_context.Blogs.Find(id) == null)
                {
                    return null;
                }
                else
                {
                    throw;
                }
            }
            return blog;
        }

        public IEnumerable<object> GetAllBlogs()
        {
            if (_context.Blogs == null)
            {
                return null;
            }
            return _context.Blogs.Include(x => x.category).Include(x => x.Positions).ToList();
        }

        public object GetBlogById(int id)
        {
            if (_context.Blogs == null)
            {
                return null;
            }
            return _context.Blogs.Where(x => x.Id == id).Include(x => x.category).Include(x => x.Positions).First();
        }


        public void Save()
        {
            _context.SaveChanges();
        }

        public void AddPosition(List<int> listPosition, Blog blog)
        {
            foreach (var id in listPosition)
            {
                if (id < LISTPOS.Count)
                {
                    Position position = new Position()
                    {
                        Id = 0,
                        Name = LISTPOS[id - 1].ToString(),
                        BlogId = blog.Id
                    };
                    _context.Positions.Add(position);
                }
            }
        }
    }
}
