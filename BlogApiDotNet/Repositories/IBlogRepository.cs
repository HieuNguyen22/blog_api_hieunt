﻿using BlogApiDotNet.DTO;
using System.Collections.Generic;
using System;

namespace BlogApiDotNet.Repositories
{
    public interface IBlogRepository : IDisposable
    {
        IEnumerable<Object> GetAllBlogs();
        Object GetBlogById(int id);
        Object CreateBlog(BlogDTO blogDTO);
        Object EditBlogById(int id, BlogDTO blogDTO);
        Object DeleteBlogById(int id);
        void Save();
    }
}
